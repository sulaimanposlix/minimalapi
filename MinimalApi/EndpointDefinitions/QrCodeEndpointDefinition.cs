﻿using MinimalApi.Models;
using MinimalApi.Repositories;
using MinimalApi.SecretSauce;

namespace MinimalApi.EndpointDefinitions
{
    public class QrCodeEndpointDefinition : IEndpointDefinition
    {
        public void DefineEndpoints(WebApplication app)
        {
            app.MapGet("/qrcodes", GetAllQrCodes);
            app.MapGet("/qrcodes/{id}", GetQrCodeById);
            app.MapPost("/qrcodes", CreateQrCode);
        }

        internal List<QrCode> GetAllQrCodes(QrCodeRepository repo)
        {
            return repo.GetAll();
        }

        internal IResult GetQrCodeById(QrCodeRepository repo, Guid id)
        {
            var qrcode = repo.GetById(id);
            return qrcode is not null ? Results.Ok(qrcode) : Results.NotFound();
        }

        internal IResult CreateQrCode(QrCodeRepository repo, QrCode qrCode)
        {
            repo.Create(qrCode);
            return Results.Created($"/qrcode/{qrCode.Id}", qrCode);
        }

        public void DefineServices(IServiceCollection services)
        {
            services.AddSingleton<QrCodeRepository>();
        }
    }
}
