﻿using MinimalApi.Models;
using MinimalApi.Repositories;
using MinimalApi.SecretSauce;

namespace MinimalApi.EndpointDefinitions
{
    public class CustomerEndpointDefinition : IEndpointDefinition
    {
        public void DefineEndpoints(WebApplication app)
        {
            app.MapGet("/customers", GetAllCustomers);
            app.MapGet("/customers/{id}", GetCustomerById);
            app.MapPost("/customers", CreateCustomer);
            app.MapPut("/customers/{id}", UpdateCustomer);
            app.MapDelete("/customers/{id}", DeleteCustomer);
        }

        internal List<Customer> GetAllCustomers(CustomerRepository repo)
        {
            return repo.GetAll();
        }

        internal IResult GetCustomerById(CustomerRepository repo, Guid id)
        {
            var customer = repo.GetById(id);
            return customer is not null ? Results.Ok(customer) : Results.NotFound();
        }

        internal IResult CreateCustomer(CustomerRepository repo, Customer customer)
        {
            repo.Create(customer);
            return Results.Created($"/customers/{customer.Id}", customer);
        }

        internal IResult UpdateCustomer(CustomerRepository repo, Guid id, Customer updateCustomer)
        {
            var customer = repo.GetById(id);
            if (customer is null)
            {
                return Results.NotFound();
            }
            repo.Update(updateCustomer);
            return Results.Ok(updateCustomer);
        }

        internal IResult DeleteCustomer(CustomerRepository repo, Guid id)
        {
            repo.Delete(id);
            return Results.Ok();
        }

        public void DefineServices(IServiceCollection services)
        {
            services.AddSingleton<CustomerRepository>();
        }
    }
}
