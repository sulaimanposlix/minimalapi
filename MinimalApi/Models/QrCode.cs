﻿namespace MinimalApi.Models
{
    // record QrCode (Guid Id, string QrText, string? Base46Qr);

    public class QrCode
    {
        public Guid Id { get; set; }
        public string? QrText { get; set; }
        public string? logo { get; set; }
        public string? Base46Qr { get; set; }
    }
}
