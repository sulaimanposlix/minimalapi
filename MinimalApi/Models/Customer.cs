﻿namespace MinimalApi.Models
{
    record Customer(Guid Id, string FullName);
}
