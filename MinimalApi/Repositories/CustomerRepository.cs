﻿using MinimalApi.Models;
using MySql.Data.MySqlClient;
using System.Data;

namespace MinimalApi.Repositories
{
    class CustomerRepository
    {
        private readonly Dictionary<Guid, Customer> _customers = new();
        private readonly IConfiguration _configuration;

        public CustomerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Create(Customer? customer)
        {
            if (customer is null)
            {
                return;
            }

            _customers[customer.Id] = customer;
        }

        public Customer? GetById(Guid id)
        {
            return _customers.GetValueOrDefault(id);
        }

        public List<Customer> GetAll()
        { // make sure connnection to phpmyadmin correct
            string query = @"select * from users";
            DataTable dt = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("UserCon");
            MySqlDataReader mySqlDataReader = null;
            using (MySqlConnection mySqlConnection = new MySqlConnection(sqlDataSource))
            {
                mySqlConnection.Open();
                using (MySqlCommand mySqlCommand = new MySqlCommand(query, mySqlConnection))
                {
                    mySqlDataReader = mySqlCommand.ExecuteReader();
                    dt.Load(mySqlDataReader);

                    mySqlDataReader.Close();
                    mySqlConnection.Close();
                }
            }
            return _customers.Values.ToList();
        }

        public void Update(Customer customer)
        {
            var existingCustomer = GetById(customer.Id);
            if(existingCustomer != null)
            {
                return;
            }

            _customers[customer.Id] = customer;
        }

        public void Delete(Guid id)
        {
            _customers.Remove(id);
        }
    }
}
