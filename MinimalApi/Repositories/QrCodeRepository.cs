﻿using MinimalApi.Models;
using System.Drawing;
using System.Drawing.Imaging;
using ZXing.QrCode;

namespace MinimalApi.Repositories
{
    class QrCodeRepository
    {
        private readonly Dictionary<Guid, QrCode> _qrCode = new();

        public void Create(QrCode? qrCode)
        {
            if (qrCode is null)
            {
                return;
            }

            qrCode = GenerateQR(qrCode);

            _qrCode[qrCode.Id] = qrCode;
        }

        public QrCode? GetById(Guid id)
        {
            return _qrCode.GetValueOrDefault(id);
        }

        public List<QrCode> GetAll()
        {
            return _qrCode.Values.ToList();
        }

        private QrCode GenerateQR(QrCode qrCode)
        {
            Byte[] byteArray;
            var width = 250; // width of the Qr Code
            var height = 250; // height of the Qr Code
            var margin = 0;
            var qrCodeWriter = new ZXing.BarcodeWriterPixelData
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Height = height,
                    Width = width,
                    Margin = margin
                }
            };
            var pixelData = qrCodeWriter.Write(qrCode.QrText);

            using (var bitmap = new Bitmap(pixelData.Width, pixelData.Height, PixelFormat.Format32bppRgb))
            {
                using (var ms = new MemoryStream())
                {
                    var bitmapData = bitmap.LockBits(new Rectangle(0, 0, pixelData.Width, pixelData.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
                    try
                    {
                        // we assume that the row stride of the bitmap is aligned to 4 byte multiplied by the width of the image
                        System.Runtime.InteropServices.Marshal.Copy(pixelData.Pixels, 0, bitmapData.Scan0, pixelData.Pixels.Length);
                    }
                    finally
                    {
                        bitmap.UnlockBits(bitmapData);
                        if (!String.IsNullOrEmpty(qrCode.logo))
                        {
                            qrCode.logo = "Not Implemented Yet !";
                        }
                    }
                    // save to stream as PNG
                    bitmap.Save(ms, ImageFormat.Png);
                    byteArray = ms.ToArray();
                    string qrBase64 = Convert.ToBase64String(byteArray, Base64FormattingOptions.None);
                    qrCode.Base46Qr = qrBase64;
                }
            }

            return qrCode;
        }
    }
}
